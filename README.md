### Basic info ###

Repozytorium zawiera materiały i kod przygotowany dla zajęć z przedmiotu Systemy Mobilne w roku akademickim 2014/15 na AGH. Można tu znaleźć projekty z laboratoriów, materiały do ich przygotowania oraz prezentację z wykładu.

*This repository contains code written for Mobile Systems classes. Here you can find presentation from my lecture and other materials related to iOS programming classes (in Polish).* 

### Wykład ###
* [Prezentacja z wykładu](https://bitbucket.org/karmel/sm-labs-ios/downloads/Wyklad%20iOS.pdf)

### Laboratorium 1 - Objective C ###

Projekt polega na stworzeniu aplikacji Zegara, która zmienia kolor tła w zależności od wyświetlanej godziny. Pozwala na zapoznanie się z podstawowymi mechanizmami tworzenia aplikacji dla iOS w środowisku XCode.

* [Instrukcja](https://bitbucket.org/karmel/sm-labs-ios/downloads/Instrukcja.pdf)
* [Prezentacja do laboratorium](https://bitbucket.org/karmel/sm-labs-ios/downloads/Laboratorium%202%20v1.1.pptx)

### Laboratorium 2 - Objective C ###

Projekt prostej aplikacji do wyświetlania aktualnej pogody oraz prognozy na nadchodzące godziny. Wykorzystuje CoreLocation oraz MapKit.

* [Instrukcja](https://bitbucket.org/karmel/sm-labs-ios/downloads/Instrukcja.pdf?pk=602550)
* [Prezentacja do laboratorium](https://bitbucket.org/karmel/sm-labs-ios/downloads/Laboratorium%202.pptx)
* [Zasoby](https://bitbucket.org/karmel/sm-labs-ios/downloads/Resources.zip)

### Laboratorium 3 - Swift ###

Projekt 

* [Instrukcja](https://bitbucket.org/karmel/sm-labs-ios/downloads/Instrukcja.pdf?pk=607187)
* [Prezentacja](https://bitbucket.org/karmel/sm-labs-ios/downloads/Laboratorium%203.pptx)
