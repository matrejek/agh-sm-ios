//
//  ViewController.m
//  Weather
//
//  Created by Matrejek, Mateusz on 21/04/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import "ViewController.h"
#import "OWMWeatherAPI.h"


@interface ViewController () {
    OWMWeatherAPI *weatherAPI;
    
    NSArray *forecast;
    
    
    NSDateFormatter *dateFormatter;

    CLLocationManager *locationManager;
    CLLocationCoordinate2D location;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (nil == locationManager) {
        locationManager = [[CLLocationManager alloc] init];
        [locationManager requestWhenInUseAuthorization];
    }

    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.distanceFilter = 500; // meters

    self.forecastTableView.dataSource = self;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    forecast = @[];
    
    weatherAPI = [[OWMWeatherAPI alloc] initWithAPIKey:@"3e38fa7f277e85749f0c10c24366a51a"];
    [weatherAPI setTemperatureFormat:kOWMTempCelcius];

    [locationManager startUpdatingLocation];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateCurrentWeatherData];
    [self updateForecastData];
}

- (IBAction)onButtonClicked:(id)sender {
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem openInMapsWithLaunchOptions:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)updateCurrentWeatherData {

    [weatherAPI currentWeatherByCoordinate:location
                              withCallback:^(NSError *error, NSDictionary *result) {

                                if (error) {
                                    NSLog(@"Error on downloading forecast: %@", [error localizedDescription]);
                                    return;
                                }

                                self.cityName.text = [NSString stringWithFormat:@"%@, %@", result[@"name"], result[@"sys"][@"country"]];
                                self.currentTemp.text = [NSString stringWithFormat:@"%.1f℃", [result[@"main"][@"temp"] floatValue]];
                                self.currentTimestamp.text = [dateFormatter stringFromDate:result[@"dt"]];
                                self.weather.text = result[@"weather"][0][@"description"];
                              }];
}

- (void)updateForecastData {
    [weatherAPI forecastWeatherByCoordinate:location
                               withCallback:^(NSError *error, NSDictionary *result) {

                                 if (error) {
                                     NSLog(@"Error on downloading forecast: %@", [error localizedDescription]);
                                     return;
                                 }

                                 forecast = result[@"list"];
                                 [self.forecastTableView reloadData];

                               }];
}

#pragma mark - UITableViewDataSource protocol

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Next Hours Forecast";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return forecast.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }

    NSDictionary *forecastData = [forecast objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%.1f℃ - %@", [forecastData[@"main"][@"temp"] floatValue], forecastData[@"weather"][0][@"main"]];
    cell.detailTextLabel.text = [dateFormatter stringFromDate:forecastData[@"dt"]];
    
    
    cell.imageView.image = [UIImage imageNamed:forecastData[@"weather"][0][@"icon"]];
    return cell;
}

#pragma mark - CLLocationManagerDelegate protocol

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject];
    location = currentLocation.coordinate;
    [self updateCurrentWeatherData];
    [self updateForecastData];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError: %@", error);
}

@end
