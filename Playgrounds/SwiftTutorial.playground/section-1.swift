let teamA : Int = 60
println(teamA.dynamicType)

let teamB : Int = 17
println(teamB.dynamicType)

var totalTeam = teamA + teamB
println(totalTeam.dynamicType)

totalTeam += 1

// Define three variables

var strVar = "string"
var intVar = 1
var floVar = 2.0

// Define three constants


let int: Int = 1


let priceInferred = 19.99
let priceExplicit: Double = 19.99

let onSaleInferred = true
let onSaleExplicit: Bool = false

let nameInferred = "Whoopie Cushion"
let nameExplicit: String = "Whoopie Cushion"

if onSaleInferred {
    println("\(nameInferred) on sale for \(priceInferred)!")
} else {
    println("\(nameInferred) at regular price: \(priceInferred)!")
}

let str: String = "string"
let flo: Double = 2.0

let s = str + " " + "\(flo)"

// Create one array and one dictionary.
var arr = [1, 2, 3, 4]
var dic = ["a": 1, "b": 2, "c": 3]


arr = arr + [5]
dic["d"] = 4



